# Nginx prometheus proxy

This Nginx proxy will be configured as a proxy for the Discourse Prometheus instance.

It uses basic authentication, so that then we can easily hook it up with our Discourse Grafana instance.

## Deployment

First, to deploy your Nginx proxy, follow the documentation under <https://paas.docs.cern.ch/2._Deploy_Applications/Deploy_From_Git_Repository/2-deploy-s2i-app/>, by selecting Nginx as the source builder and setting this repository as the repository source.

Then, deploy the `prom/prometheus` docker in OpenShift image by following the <https://paas.docs.cern.ch/2._Deploy_Applications/Deploy_Docker_Image/1-deploy-private-docker/#deploy-a-docker-image-in-openshift> documentation.

We just need to adjust the configuration of both deployments.

## Configure Nginx proxy

We need to setup the user and password for the basic authentication in the Nginx proxy. Let's follow the documentation under <https://prometheus.io/docs/guides/basic-auth/>

We need to create a secret as follows (note the key must be `.htpasswd`, in consonance with the element `auth_basic_user_file` under the `nginx.conf` configuration file):

```bash
oc project <your-project>

echo "
kind: Secret
apiVersion: v1
metadata:
  name: htpasswd-secret
stringData:
  .htpasswd: '<name>:<hashed value>'
" | oc apply -f -
```

Then, we just need to populate it to the Nginx deployment.

```yaml
...
template:
  spec:
    volumes:
      - name: htpasswd-secret
        secret:
          secretName: htpasswd-secret
    containers:
      - name: nginx-prometheus-proxy
        ...
        volumeMounts:
          - name: htpasswd-secret
            mountPath: /opt/app-root/etc/nginx.d/
```

> n.b.: keep safe this password, since it will be used when configuring the Prometheus datasource from Grafana.

## Configure Prometheus

Prometheus expects having a configmap called `prometheus.yaml` in which we can specify which targets Prometheus should scrape.

Create the `prometheus-configmap` configmap by filling with the data you want to scrap:

```bash
oc project <your-project>

echo "
kind: ConfigMap
apiVersion: v1
metadata:
  name: prometheus-configmap
data:
  prometheus.yml: |
    global:
      scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
      evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
      # scrape_timeout is set to the global default (10s).
      
    # Alertmanager configuration
    alerting:
      alertmanagers:
      - static_configs:
        - targets:
      # - alertmanager:9093
      
    # Load rules once and periodically evaluate them according to the global evaluation_interval

    rule_files:
    # - "first_rules.yml"
    # - "second_rules.yml"

    # A scrape configuration containing exactly one endpoint to scrape:
    # Here it's Prometheus itself.

    scrape_configs:
    # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
    - job_name: 'prometheus'
      # metrics_path defaults to '/metrics'
      # scheme defaults to 'http'.
      static_configs:
      - targets: ['localhost:9090']

    # Add your endpoints for scraping
    - job_name: 'my-site'
      scheme: https # change to http if don't you have https
      metrics_path: '/metrics'
      static_configs:
      - targets: ['my-site.web.cern.ch'] # host and port you serve
" | oc apply -f -
```

And populate it to the `Prometheus` Deployment:

```yaml
...
template:
  spec:
    volumes:
      - name: prometheus-configmap-ksm3y
        configMap:
          name: prometheus-configmap
    containers:
      - name: prometheus
        ...
        volumeMounts:
          - name: prometheus-configmap-ksm3y
            readOnly: true
            mountPath: /etc/prometheus/
...
```

Create a pvc called `prometheus` and populate it to the Deployment as well.

```yaml
...
template:
  spec:
    volumes:
      - name: prometheus
        persistentVolumeClaim:
          claimName: prometheus
      - name: prometheus-configmap-ksm3y
        configMap:
          name: prometheus-configmap
    containers:
      - name: prometheus
      ...
      volumeMounts:
        - name: prometheus
          mountPath: /prometheus
        - name: prometheus-configmap-ksm3y
          readOnly: true
          mountPath: /etc/prometheus/
...
```

And it's done, you should be able to access your Discourse Prometheus instance.
